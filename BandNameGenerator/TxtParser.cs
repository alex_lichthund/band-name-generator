using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace BandNameGenerator
{
    public class TxtParser
    { 
        //Regex r = new Regex("[a-zA-Z]+");
        //simple parser
        public static List<Pattern> ParsePatterns(string path, Dictionary<string, List<string>> wordsDatabase ,bool web = false)
        {
            List<Pattern> patterns = new List<Pattern>();

            string text = System.IO.File.ReadAllText(path);
            text = Regex.Replace(text, "#(.|\\n)*#", "");

            var symbols = text.Split();
            Pattern currentPattern = new Pattern();
            
            foreach (string symbol in symbols)
            {
                if (string.IsNullOrEmpty(symbol))
                {
                    continue;
                }
                
                if (symbol[0] == '$')
                {
                    var val = symbol.Trim('$');
                    int intVal = Int32.Parse(val);
                    if (currentPattern != null)
                    {
                        currentPattern.weight = intVal;
                        currentPattern.ApplyArgs();
                        patterns.Add(currentPattern);
                        currentPattern = new Pattern();
                    }
                }
                else if (symbol[0] == '-')
                {
                    var val = symbol.Trim('-');
                    if (currentPattern.elements.Last() != null)
                    {
                        currentPattern.elements.Last().args.Add(val);
                    }
                }
                else
                {
                    if(!wordsDatabase.ContainsKey(symbol))
                    {
                        throw new Exception("No words in database that are connected to: " + symbol + " class. Check for typos!");
                        continue;
                    }

                    Element newElement = new Element(wordsDatabase[symbol]);

                    if (currentPattern != null)
                    {
                        currentPattern.elements.Add(newElement);
                    }
                }
            }

            return patterns;
        }

        public static T CreateInstance<T>(params object[] paramArray)
        {
            return (T)Activator.CreateInstance(typeof(T), args: paramArray);
        }
    }
}