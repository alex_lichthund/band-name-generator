using System.Collections.Generic;
using System.Linq;
using Pluralize.NET;

namespace BandNameGenerator
{
    public class Element
    {
        public List<string> wordDatabase = new List<string>();
        public List<string> args = new List<string>();

        public Element(List<string> data)
        {
            wordDatabase = data;
        }

        public void ApplyArgs()
        {
            foreach (var arg in args)
            {
                switch (arg)
                {
                        case "p":
                            MakePlural();
                            break;
//                        case "l":
//                            MakeLowercase();
//                            break;
//                        case "u":
//                            MakeUppercase();
//                            break;
                }
            }
        }

        private void MakeUppercase()
        {
            for (int i = 0; i < wordDatabase.Count; i++)
            {
                wordDatabase[i] = char.ToUpper(wordDatabase[i].First()) + wordDatabase[i].Substring(1);
            }
        }

        private void MakeLowercase()
        {
            for (int i = 0; i < wordDatabase.Count; i++)
            {
                wordDatabase[i] = wordDatabase[i].ToLower();
            }
        }

        private void MakePlural()
        {
            for (int i = 0; i < wordDatabase.Count; i++)
            {
                wordDatabase[i] = new Pluralizer().Pluralize(wordDatabase[i]);
            }
        }
    }
}