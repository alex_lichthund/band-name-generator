using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using SimpleJSON;

namespace BandNameGenerator
{
    public class NameGenerator
    {
        private List<Pattern> patterns;
        private Random random;

        private const string PATTERNS_PATH = "WordsData/Patterns.txt";
        private const string WORDS_PATH = "WordsData/Data.json";
        private string SERVER_PATTERNS_PATH;
        private bool isWeb = false;


        public NameGenerator(bool isWeb = false)
        {
            this.isWeb = isWeb;
            random = new Random();

            string path = PATTERNS_PATH;
            string wordsPath = WORDS_PATH;

            if(HttpContext.Current != null)
            {
                SERVER_PATTERNS_PATH = HttpContext.Current.Request.MapPath("~/WordsData/Patterns.txt");
                path = isWeb ? SERVER_PATTERNS_PATH : PATTERNS_PATH;
                wordsPath = isWeb ? HttpContext.Current.Request.MapPath("~/WordsData/Data.json") : WORDS_PATH;
            }
           
            var wordsDict = PrepareWordsDatabase(wordsPath);

            patterns = TxtParser.ParsePatterns(path, wordsDict, isWeb);
        }

        public Dictionary<string, List<string>> PrepareWordsDatabase(string path)
        {
            Dictionary<string, List<string>> wordsDatabase = new Dictionary<string, List<string>>();

            var jsonDatabase = File.ReadAllText(path);
            var cardsJson = JSON.Parse(jsonDatabase.ToString());
            foreach (var entity in cardsJson)
            {
                foreach (var entityVal in entity.Value)
                {
                    string word = new Regex("[a-zA-Z]+").Match(entityVal.Value).Value.ToUpper();

                    if (!wordsDatabase.ContainsKey(entityVal.Key))
                    {
                        wordsDatabase.Add(entityVal.Key, new List<string> { word });
                    }
                    else
                    {
                        wordsDatabase[entityVal.Key].Add(word);
                    }
                }
            }

            return wordsDatabase;
        }

        public List<string> Generate(int quantity)
        {
            var list = new List<string>();

            for (int i = 0; i < quantity; i++)
            {
                Pattern pattern = RouletteSpinSelection(patterns);
                string name = "";

                if (pattern != null)
                {
                    foreach (Element element in pattern.elements)
                    {
                        if (element != null)
                        {
                            if(element.wordDatabase.Count == 0)
                            {
                                continue;
                            }

                            name += GetRandomItem(element.wordDatabase) + " ";
                        }
                        else
                        {
                            name += "";
                        }
                    }
                }
                list.Add(name);
            }

            return list;
        }
        
        public Pattern RouletteSpinSelection(List<Pattern> patterns)
        {
            int rouletteSpin = random.Next(0, patterns.Select(x => x.weight).Sum());

            int weightSum = 0;
            foreach (var pattern in patterns)
            {
                weightSum += pattern.weight;

                if (weightSum >= rouletteSpin)
                {
                    return pattern;
                }
            }

            return null;
        }
        
        public T GetRandomItem<T>(ICollection<T> collection)
        {
            int random = this.random.Next(0, collection.Count);
            return collection.ElementAt(random);
        }
    }
}