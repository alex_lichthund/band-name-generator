using System;
using System.Collections.Generic;

namespace BandNameGenerator
{
    public class Pattern
    {
        public List<Element> elements = new List<Element>();
        public int weight;

        public void ApplyArgs()
        {
            foreach (var element in elements)
            {
                if(element != null)
                {
                    element.ApplyArgs();
                }
            }
        }
    }
}