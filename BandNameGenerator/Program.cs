﻿using System;
using System.IO;

namespace BandNameGenerator
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Type number of names to generate: ");
            int namesQuantity = Convert.ToInt32(Console.ReadLine());
            
            var nameGenerator = new NameGenerator();
            var names = nameGenerator.Generate(namesQuantity);
            
            TextWriter tw = new StreamWriter("GeneratedNames.txt");

            foreach (String s in names)
            {
                Console.WriteLine(s);
                tw.WriteLine(s);                
            }

            tw.Close();
            
            Console.WriteLine("");
            Console.WriteLine("Done! Names generated to file: GeneratedNames.txt");
            Console.ReadKey();
        }
    }
}