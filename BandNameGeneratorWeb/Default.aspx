﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="BandNameGeneratorWeb._Default" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <!--
    <div class="jumbotron">
        <h1>ASP.NET</h1>
        <p class="lead">ASP.NET is a free web framework for building great Web sites and Web applications using HTML, CSS, and JavaScript.</p>
        <p><a href="http://www.asp.net" class="btn btn-primary btn-lg">Learn more &raquo;</a></p>
    </div>
    -->
    <!--MasterPageFile="~/Site.Master"-->
    <div class="container">
  <h1>Band Names Generator</h1>
  <div class="row">
    <div class="col-sm-9" style="background-color:yellow;">
      <div>
        <h3>Pattens:</h3>
      </div>
      <div>
          <asp:TextBox ID="TextBox4" runat="server" Height="250px" Width="700px" TextMode="MultiLine" Wrap="false" ReadOnly="true" BackColor="Silver" ForeColor="Black"></asp:TextBox>
      </div>
      <div>
        <asp:TextBox ID="TextBox1" runat="server" Height="250px" Width="700px" TextMode="MultiLine" Wrap="false"></asp:TextBox>
      </div>
    </div>
    <div class="col-sm-3" style="background-color:pink;">
      <div>
          <h3>
          Output text:
          </h3>
      </div>
      <div>
        <asp:TextBox ID="TextBox2" runat="server" Height="500px" Width="200px" TextMode="MultiLine" Wrap="false"></asp:TextBox>
      </div>
    </div>
  </div>
    <div class="row">
        <div><h3>Names quantity:</h3></div>
        <asp:TextBox ID="TextBox3" runat="server" Height="35px" Width="210px" TextMode="Number" Text="100"></asp:TextBox>
         <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Generate" />
        <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Save output to disc" />
        <asp:Button ID="Button3" runat="server" OnClick="Button3_Click" Text="Save Patterns" Width="103px" />
    </div>
</div>

</asp:Content>
