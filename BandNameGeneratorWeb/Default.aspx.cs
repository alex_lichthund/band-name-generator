﻿using BandNameGenerator;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;

namespace BandNameGeneratorWeb
{
    public partial class _Default : Page
    {
        private List<string> names = new List<string>();

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.LoadComplete += new EventHandler(Page_LoadComplete);
            TextBox4.Text =
@"####################################################################

Just type calsses of words that should be included in pattern and end it with $ and pattern weight(Examples below).

Available calsses of words:
(See 1st column in google sheet) Examples: AnimalNoun, The, Noun etc.
                    
Available arguments(add afeter class name):
-p	-make word plural

####################################################################";
        }

        void Page_LoadComplete(object sender, EventArgs e)
        {
            TextBox1.Text = File.ReadAllText(HttpContext.Current.Request.MapPath("~/WordsData/Patterns.txt"));
        }

        public void DebugLabel(string debug)
        {
            TextBox4.Text = debug;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            int quantity = Int32.Parse(TextBox3.Text);

            NameGenerator nameGenerator = null;
            try
            {
                nameGenerator = new NameGenerator(true);
            }
            catch (Exception exception)
            {
                TextBox4.Text = exception.Message;
                return;
            }

            names = nameGenerator.Generate(quantity);

            var displayNames = "";
            foreach (var name in names)
            {
                displayNames += name + "\n";
            }

            TextBox2.Text = displayNames;
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            string output = TextBox2.Text;
            sb.Append(output);
            sb.Append("\r\n");

            string text = sb.ToString();

            Response.Clear();
            Response.ClearHeaders();

            Response.AppendHeader("Content-Length", text.Length.ToString());
            Response.ContentType = "text/plain";
            Response.AppendHeader("Content-Disposition", "attachment;filename=\"GeneratedNames.txt\"");

            Response.Write(text);
            Response.End();
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            File.WriteAllText(HttpContext.Current.Request.MapPath("~/WordsData/Patterns.txt"), TextBox1.Text);
        }
    }
}