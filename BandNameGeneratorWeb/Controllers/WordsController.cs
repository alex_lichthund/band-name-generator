﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;

namespace BandNameGeneratorWeb.Controllers
{
    public class WordsController : ApiController
    {
        string val = "val";
        // GET: api/Words
        public IEnumerable<string> Get()
        {
            return new string[] { val };
        }

        // GET: api/Words/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Words
        [HttpPost]
        public async Task<HttpResponseMessage> Post(HttpRequestMessage request)
        {
            var jsonString = await request.Content.ReadAsStringAsync();

            string json1 = HostingEnvironment.MapPath(@"~/WordsData/Data.json");
            File.WriteAllText(json1, jsonString);

            return new HttpResponseMessage(HttpStatusCode.Created);
        }

        // PUT: api/Words/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Words/5
        public void Delete(int id)
        {
        }
    }
}
